import bpy
import os
from pathlib import Path

input_dir = '/path/to/your/blend/files'
input_ext = 'blend'
output_dir = '/path/to/your/glb/files'
output_ext = 'glb'


################################################################################
# Clear all objects in scene

def clear_objects():
    # selecting all scene objects
    bpy.ops.object.select_all(action='SELECT')

    # deleting all scene objects
    bpy.ops.object.delete()


################################################################################
# Effectively 'reset' the scene/file entity hierarchy

def clear_entities():
    context = bpy.context
    scene = context.scene
    for c in scene.collection.children:
        scene.collection.children.unlink(c)

    for c in bpy.data.collections:
        if not c.users:
            bpy.data.collections.remove(c)

    for key in bpy.data.meshes.keys():
        entity = bpy.data.meshes[key]
        bpy.data.meshes.remove(entity)

    for key in bpy.data.materials.keys():
        entity = bpy.data.materials[key]
        bpy.data.materials.remove(entity)

    for key in bpy.data.brushes.keys():
        entity = bpy.data.brushes[key]
        bpy.data.brushes.remove(entity)

    for key in bpy.data.images.keys():
        entity = bpy.data.images[key]
        bpy.data.images.remove(entity)

    for key in bpy.data.worlds.keys():
        entity = bpy.data.worlds[key]
        bpy.data.worlds.remove(entity)

    for key in bpy.data.linestyles.keys():
        entity = bpy.data.linestyles[key]
        bpy.data.linestyles.remove(entity)

    for key in bpy.data.scenes.keys():
        entity = bpy.data.scenes[key]
        # don't delete the default 'Scene' scene
        if entity.name != 'Scene':
            bpy.data.scenes.remove(entity)


################################################################################
# Import attributes from given file into this file

def import_attributes(import_file):
    with bpy.data.libraries.load(import_file) as (data_from, data_to):
        for attr in dir(data_to):
            setattr(data_to, attr, getattr(data_from, attr))


################################################################################
# Move objects to the main scene collection

def move_objects_to_scene_collection():
    coll = bpy.context.scene.collection
    for key in bpy.data.objects.keys():
        entity = bpy.data.objects[key]
        usercol = entity.users_collection
        coll.objects.link(entity)
        for col in usercol:
            col.objects.unlink(entity)


################################################################################
# Batch convert files in a given directory from .blend to .glb

def batch_convert():
    count = 0

    for root, dirs, files in os.walk(input_dir):
        for name in files:
            if not name.endswith(input_ext):
                continue

            clear_objects()

            # assemble the new file path
            import_file = os.path.join(root, name)
            print('===================================================================================================')
            print('Importing ' + import_file)

            clear_objects()

            # import the existing data into the current scene
            import_attributes(import_file)

            # relocate objects under the scene collection before exporting
            move_objects_to_scene_collection()

            # export all content to gltf
            export_file = os.path.join(output_dir, Path(name).stem + '.' + output_ext)
            print('Exporting ' + export_file + ' (' + str(count) + ' exported)')
            count = count + 1
            bpy.ops.export_scene.gltf(filepath=export_file)

            clear_entities()
            clear_objects()


batch_convert()
