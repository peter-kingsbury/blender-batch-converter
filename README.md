# Blender Batch Converter

This script, when imported into a Blender scripting workspace, will batch-convert `.blend` files into `.glb`.

## Prerequisites

* Blender 2.9

## Usage

1. Start a new Blender session.
1. Go to the `Scripting` tab.
1. Create a new script by clicking `+ New`.
1. Paste the contents of `blender-batch-converter.py`.
1. Update the variables `input_dir` and `output_dir`.
1. Click the `Run Script` button.

Your `.blend` files should now be converted to `.glb` and appearing in `output_dir`.

## Bugs & Known Limitations

1. I am not a Python developer, or a Blender guru; I needed a script for [Godot](https://godotengine.org/) game development, so this script was born. Some of the code is embarrassingly fugly, but it works. 🙂 
1. This will flatten any `.blend`-file directory structure for the output `.glb` files, but will _not_ modify the original `.blend`-file directory structure. 
1. This script has only been tested on Blender 2.91.2; for any [issues](https://gitlab.com/peter-kingsbury/blender-batch-converter/-/issues/new), please submit a Pull Request.
1. Any other [improvements](https://gitlab.com/peter-kingsbury/blender-batch-converter/-/issues/new) should similarly come in the form of a Pull Request.

## License

Licensed under [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/). See [LICENSE.md](LICENSE.md) for details.
